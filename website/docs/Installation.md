---
sidebar_position: 1.1
---

Installation can be delivered in two options:
  * As GrapEditor setup, usually as GrapvvvvSetup.exe, where vvvv denotes version of GrapEditor
    * with separate setup for TSP 
  * As GrapDesigner setup, usually as GrapvvvvTargetSetup.exe, where Target is name of target system.

In both cases GrapEditor will ask for registration upon start. Currently, registration is performed by sending text from registration window to e-mail Mario.Bilic@koncar-institut.hr. Registration key is typically sent back on the same working day.

## GrapEditor setup ##
In case of GrapEditor setup, GrapTSP must be installed separately. During Setup, you will need to specify:
  * Grap.exe installation directory, default is C:\Program Files (x86)\Grap5.
  * Do you want Grap shortcut on desktop.
After installed, on start, GrapEditor will show message "Project not defined" and open "blank" page. In that case, if this is very first installation of Grap on the computer, close it and install TSP.

When Grap is showing "blank" page and some Project and corresponding TSP exists on disk, it can be selected by Project->Open. If Project does not exists but TSP does, new Project can be created by Project->Crete New.

GrapEditor setup may be used as upgrade of older version of GrapEditor. In that case, after installation, GrapEditor will probably automatically open last opened Application and Project, or Project can be selected by Project->Open.

## GrapTSP setup ##
In case of separate TSP setup, Grap should be closed during setup. During Setup, you will need to specify the following:
  * Location for the TSP directory, default is C:\GrapTSPs\//Target//, where //Target// is the name of the target system.
  * If demo Project with demo Application is to be installed, and if yes:
    * Location for demo project.
  * If a new Project is to be installed, and if yes
    * Location and name for the new Project.
If you choose not to install new Project, it can be easily created from Grap by Project->Crete New.
 

## GrapDesigner setup ##
In case of GrapDesigner setup, GrapTSP for particular target can be installed together with GrapEditor form the single setup. That will automatically create demo Project including demo Application. Demo Application can simply be compiled and, if Target is available, programed and started. During Setup, you will need to specify the following:
  * Grap.exe installation directory, default is C:\Program Files (x86)\Grap5.
  * Grap shortcut on desktop, if wanted.
  * If TSP is to be installed and if yes, proceed as in separate [[usermanuals:grap_designer:installation#GrapTSP setup|TSP setup]].
