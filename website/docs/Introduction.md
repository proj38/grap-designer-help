---
sidebar_position: 1
slug: /
---

# Introduction

Grap®Designer is MS Windows graphical programing tool for embedded systems similar to Function Block Diagram (FBD) from IEC 61131-3, cf. Programming Tutorial at plcacademy.com
In addition to designing application, it enables live monitoring and parametrizing of running Application.

Grap® is the umbrella term for all KEEI software related to embedded systems.

Proprietary Grap®OS on target system is precondition for Grap applications to run on target processor.

This manual mainly covers Grap®Editor, but since GrapEditor is unusable without at least one TSP, manual is called GrapDesigner User Manual. Sometimes term Grap alone is used for GrapEditor.