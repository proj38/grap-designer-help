---
sidebar_position: 2
slug: /main-window
---

# Main Window #

GrapEditor main window is used for showing and editing function block diagrams of Modules and Blocks. Diagrams are designed from Elements (function blocks) connected with wires. 
## Title Bar ##
Window title bar, besides program name, contains important information about opened Module or Block in following format:
 
Grap //version// - //ProjectName//://ApplicationName// - Module: //ModuleName CurrentSheet/SheetNumber //
## Tool Bar ##
[![graptoolbar](/images/graptoolbar.png)](#)

[![app](/images/app.png)](#) Show Application window

[![new](/images/new.png)](#) New Module

[![open](/images/open.png)](#) Open Module or Block

[![save](/images/save.png)](#) Save Module or Block

[![saveas](/images/saveas.png)](#) Save Module or Block as new file

[![print](/images/print.png)](#) Print current Module or Block

[![delete](/images/delete.png)](#) Delete selected item(s) from diagram

[![undo](/images/undo.png)](#) Undo single change
 
[![select](/images/select.png)](#) Select item on sheet

[![element](/images/element.png)](#) Put new element on sheet

[![browser](/images/browser.png)](#) Show element browser window

[![wire](/images/wire.png)](#) Use wire to connect elements on sheet

[![label](/images/label.png)](#) Put label on sheet

[![text](/images/text.png)](#) Put text on sheet

[![circle](/images/circle.png)](#) Draw circle on sheet

[![line](/images/line.png)](#) Draw line on sheet

[![sheetprev](/images/sheetprev.png)](#) Show previous sheet of Module or Block

[![sheetnext](/images/sheetnext.png)](#) Show next sheet of Module or Block

[![historyback](/images/historyback.png)](#) Open previous opened Module or Block from history

[![historyforward](/images/historyforward.png)](#) Go forward from in history of opened Modules and Blocks

[![translate](/images/translate.png)](#) Translate current Module or Block

[![build](/images/build.png)](#) Build Application

[![buildload](/images/buildload.png)](#) Build and load Application to Target, connect if needed

[![connect](/images/connect.png)](#) Connect to Target

## Main Menu ##
  * [File](main-window/file-menu) - actions related to diagram showed in main window
  * [Application](main-window/application-menu)- actions related to current Application
  * [Project](main-window/project-menu) - actions related to current project
  * [Blocks](main-window/blocks-menu) - actions related to blocks
  * [Draw](main-window/draw-menu) - graphical items available for drawing
  * [Edit](main-window/edit-menu) - diagram editing actions
  * [View](main-window/view-menu) - actions related to current view of diagram
  * [Windows](main-window/windows-menu) - actions related to GrapEditor windows arrangement on desktop
  * [Tools](main-window/tools-menu) - various tools
  * [Sheets](main-window/sheets-menu) - sheets actions
  * [RunTime](main-window/runtime-menu) - actions related to connection to live Target
  * [Help](main-window/help-menu) - simple help actions and About

## Status Bar ##
Status bar shows few more on less important information about GrapDesigner current status and enables some useful actions.
### Connection status ###
At leftmost side of status bar there is a "lamp" indicator of connection status to target. Color of "lamp" represent status of connection:
  * [![ledgray](/images/status_bar/ledgray.jpg)](#) Gray - "Target Status: Not connected"
  * [![ledcyan](/images/status_bar/ledcyan.jpg)](#) Cyan - "Target Status: Busy..."
  * [![ledgreen](/images/status_bar/ledgreen.jpg)](#) Green - "Target Status: Connected"
  * [![ledred](/images/status_bar/ledred.jpg)](#) Red - "Target Status: Application stopped!" (Reason may be shown)
  * [![ledyellow](/images/status_bar/ledyellow.jpg)](#) Yellow - "Target Status: Connection Error!" (Connection to Target failed)
Text in quotation marks is show in hint message when mouse hovers over lamp.
 
Click on "lamp" will open [Connection window](connection-window).

### Current zoom ###
Next to lamp, current zoom of diagram is shown in percentage. When mouse hovers over it, hint message "Current zoom" is shown.
With mouse click, zoom level can be precisely set using [number entering dialog](number-entering-dialog) which is show over status bar. 

### Cursor position ###
Next to lamp, current mouse cursor coordinates are shown. Coordinates are rounded to grid. When mouse hovers over it, hint message "Mouse position aligned to grid" is shown.

### Used memory ###
Next status bar panel shows indication of memory usage of GrapEditor in kB. When mouse hovers over it, hint message "Used memory" is shown.

### Current OE ###
Following status bar panel shows which OE number will se used for next placed element. When mouse hovers over it, hint message "Current OE number" is shown. That OE number can be changed with mouse click on panel, which will open [number entering dialog](number-entering-dialog)

