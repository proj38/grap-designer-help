---
sidebar_position: 30
---

# Application Window #

GrapDesigner Application window is used for manipulation with Modules within Application. And for making changes and building Application.
## Toolbar ##

[![app](/images/application/apptoolbar.png)](#)

[![new](/images/new.png)](#) New Application

[![open](/images/open.png)](#) Open Application

[![save](/images/save.png)](#) Save Application

[![saveas](/images/saveas.png)](#) Save Application as new file

[![print](/images/print.png)](#) Print current Application

[![build](/images/build.png)](#) Build Application

[![buildload](/images/buildload.png)](#) Build and load Application to Target, connect if needed

[![AppBtnRebuildAll](/images/application/appbtnrebuildall.png)](#) Rebuild all Modules in Application

[![AppBtnLink](/images/application/appbtnlink.png)](#) Link Application

[![AppBtnCreateTitle](/images/application/appbtncreatetitle.png)](#) Create Application title

[![AppBtnRefreshTitle](/images/application/appbtnrefreshtitle.png)](#) Refresh Application title

[![AppBtnMore](/images/application/appbtnmore.png)](#) Open menu with more Application actions 

### Application Menu


  * New
  * Open ...
  * Save ...
  * Save as ...
  * Print
  * Build
  * Build and Load
  * Rebuild all
  * Link
  * Create Title
  * Refresh Title
  * More...
    * Import from CMD
    * Copy application files to...
    * Import GRASS module
    * Copy modules from...
    * Convert Grap1 modules
    * Resave all modules