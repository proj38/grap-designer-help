---
sidebar_position: 10
slug: /understanding-tasks-and-sysdef
---

# Understanding tasks and SYSDEF #

## SYSDEF ##

The SYSDEFFL element initializes the SYSDEF area. This area contains the information that makes it possible for the operating system to manipulate the application program. The SYSDEF area contains the sample times for the levels T0, T1, T2, T3, T4, T5, T6 and T7. Maximal allowed sample time for level T0 is 26ms and maximal suggested sample time for level T7 is around 10000ms. SYSDEFFL element defines application program name and descriptions. It is also used for enabling interrupt tasks P1, P2, P3, P4, U5, U6 and U7. 

The SYSDEF area also contains other information such as where the different levels and the signal name table are located, the system error signals (see the DMK105ER element) system state signals (see the DMK105SY element), the logging channels and so on. 

The SYSDEFFL element will initiate some parts of the system at power up as: clearing the RWM areas of the application program, copying the value of parameters from MVRAM or FEPROM to their location in RWM. 
The SYSDEFFL element may only occur once in the total application program and has to be the first element. 

System constants defined in SYSDEFFL and used by the other application program modules are externally defined in TSYSXXXX and externally referenced from the application program modules. (See the element description for the elements XDEF and XREF). 

SYDEFFL element defines in symbol table operating system variables DAYS1984, MILLISEC and ETIME.

## Task scheduler working concept ##
Our real-time operating system is called GrapOS. It uses task scheduler with rate monotonic scheduling with harmonic task set. This type of task scheduler is specifically designed to provide the hard real-time guarantees. Every task executes with specific time period and has the fix priority. The task with the shortest time period has the highest priority. Generally, the greater period of the task means the lower priority and vice-versa. Time periods of the tasks with lower priorities are multiples of the time periods of the highest priority task.

[![taskscheduler](/images/task_scheduler/taskscheduler.png)](#)   

Explanation for the graph showed is given below.

### 0-10 ms on the time scale (x axle) ###

On the system start up, all tasks are initialized and ready for execution.

Since the Task 1 has the highest priority (shortest time period, 10 ms), it executes first. All other tasks are in the block state. After the 2 ms, Task 1 has finished its execution and goes back to the ready state until the next execution (it executes every 10 ms). 

For the next 8 ms Task 1 will not execute, so tasks with the lower priority can be executed. Next task to execute is the Task 2, because it has higher priority than the Task 3 and background task (it has shorter time period). It needs 6 ms to execute. So, it executes completely and goes back to the ready state. It will execute again after 14 ms (20-6 ms).

There are still 2 ms before the Task 1 executes again. Now, the Task 3 can execute. It needs 12 ms to execute, but it has only 2 ms before the Task 1 starts its second execution. What happens next is execution of the Task 3, but after 2 ms, Task 1 blocks the execution of the Task 3, and starts to execute. 

### 10-20 ms ###

After next 2 ms, Task 1 finishes its execution. There are 12 ms until the Task 2 is ready to be executed again, and 8 ms until the next execution of the Task 1. So, the Task 3 continues its execution. It needs 10 ms to finish execution. After the 8 ms, Task 1 blocks the Task 3 and starts execution.

### 20-30 ms ###

Task 1 starts to execute. It needs 2 ms to execute. After 2 ms, Task 3 could continue with its execution, but Task 2 is also ready to execute. Task 2 has higher priority and executes before the task 3. After 6 ms, execution of the Task 2 finishes. There are still 2 ms until the Task 1 needs to be executed, and Task 3 needs 2 ms to finish its execution. Finally, after 3 time periods of the Task 1, Task 3 ends with its first execution.

### 30-40 ms ###

At the beginning of this time frame there are 38 ms until the next execution of the Task 3, 12 ms until the next execution of the Task 2. Task 1 starts with the execution. After 2 ms, it finishes with execution and there are no other critical tasks to execute. Until the next execution of the Task 1, the Background task is active.

Applications for the GrapOS based systems have 8 tasks defined, T0..T7. T0 has the highest priority of execution, while the T7 has the lowest priority. Their time periods are defined within the SYSDEFFL Element. There are four more tasks, which are not used for functional Application code, but as a part of the system support. 
They are:
  * **INIT** task – part of the Application code, which is executed only once, right after the system start
  * **START** task - part of the Application code, which is executed only once, after INIT task
  * **E7** task – asynchronous task, shut down task. Multiple events can cause E7 task execution. It sets all Digital outputs to their initial state (all signals toward actuators to logical zero value)
  * **STOP** task – Application stop task. It is executed when the Application is stopped. It has minimum functionality, e.g., displaying information on the display. There can be multiple causes of the Application stop.  