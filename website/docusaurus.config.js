// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');
const math = require('remark-math');
const katex = require('rehype-katex');

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Grap®Designer User manual',
  tagline: '',
  url: 'https://proj38.gitlab.io',
  baseUrl: '/grap-designer-help/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: 'facebook', // Usually your GitHub org/user name.
  projectName: 'docusaurus', // Usually your repo name.

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'en',
    locales: ['en'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          path: 'docs',
		      routeBasePath: 'online_manual',
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [math],
          rehypePlugins: [katex],
        },
		blog: false,
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      }),
    ],
  ],
  stylesheets: [
    {
      href: 'https://cdn.jsdelivr.net/npm/katex@0.13.24/dist/katex.min.css',
      type: 'text/css',
      integrity:
        'sha384-odtC+0UGzzFL/6PNoE8rX/SPcQDXBJ+uRepguP4QkPCm2LBxH3FA3y+fKSiJ+AmM',
      crossorigin: 'anonymous',
    },
  ],
plugins: [
    [
      require.resolve("@cmfcmf/docusaurus-search-local"),
      {
        indexBlog: false
      },
    ],
    [
      '@docusaurus/plugin-content-docs',
      {
        id: 'elements',
        path: 'elements',
        routeBasePath: 'elements-library',
        sidebarPath: require.resolve('./sidebarsElements.js'),
      }, 
    ],
 ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      navbar: {
        title: 'Grap®Designer', /*
        logo: {
          alt: 'My Site Logo',
          src: '/images/graptitle.png',
        },*/
        items: [
          {
            type: 'doc',
            docId: 'Introduction',
            position: 'left',
            label: 'Online manual',
          //  activeBaseRegex: `/docs/`,
          },
          {
            docId: 'elements',
            to: '/elements-library/AAND',    // ./docs/Intro.md
            label: 'Elements Library',
            position: 'left',
           // activeBaseRegex: `/elements/`,
          },
          


     
        /**  {to: '/blog', label: 'Blog', position: 'left'},
          {
            href: 'https://github.com/facebook/docusaurus',
            label: 'GitHub',
            position: 'right',
          }, */
        ],
      },
      footer: {
        style: 'dark',
        links: [
          {
            title: 'Docs',
            items: [
              {
                label: 'Grap®Designer Online manual',
                to: '/docs/Introduction',
              },
            ],
          },
          {
            title: 'Links',
            items: [
              {
                label: 'KONČAR - Electrical Engineering Institute',
                href: 'https://www.koncar-institut.hr',
              },
              {
                label: 'KONČAR – Electrical Industry',
                href: 'https://www.koncar.hr',
              }
            ],
          },
   /**       {
            title: 'More',
            items: [
              {
                label: 'Blog',
                to: '/blog',
              },
              {
                label: 'GitHub',
                href: 'https://github.com/facebook/docusaurus',
              },
            ],
          },*/
        ],
        logo: {
          alt: 'KONČAR - Electrical Engineering Institute Logo',
          src: '/images/logo/KONCAR_INSTITUT_logo_ENG.svg',
          href: 'https://www.koncar-institut.hr',
          width: 303,
          height: 100,
        },
        copyright: `Copyright © ${new Date().getFullYear()} KONČAR - Electrical Engineering Institute Ltd.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
