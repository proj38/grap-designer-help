---

---

# AAND
Bitwise AND operation

### Application
Bitwise manipulation of integers.

### Symbol	
[![AAND](AAND.png)](#) 

### Version
0

### Variants	
* AAND	default arithmetical type
* AAND_I	signed 16-bit integers
* AAND_L	signed 32-bit integers
* AAND_Q	signed 64-bit integers

### Pin functions	
| Name | Type | Short description |
| ---- | ---- | ----------------- |
| A	| arithmetical | input	first operand |
| B	| arithmetical | input	second operand |
| OUT | arithmetical | output	output signal |

### Algorithm	
OUTk = Ak ∧Bk  

### Description
Calculates the k-th bit of output as logical AND between corresponding bits in A and B. There is no special treatment for sign bit.